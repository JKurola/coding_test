module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                src: ['public/js/jquery-3.2.1.min.js', 'public/js/jsplugin.js', 'public/js/index.js'],
                dest: 'public/js/min.js'
            },
            css: {
                src: ['public/css/bootstrap.min.css', 'public/css/index.css'],
                dest: 'public/css/min.css',
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default', ['concat']);
};
