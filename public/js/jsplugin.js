$.fn.validImgPath = function() {
    var that = this;
    var img_src;
    var img;
    if (this.is('img') && this.data('img')) {
        img_src = this.data('img');
        img = new Image();
        img.onload = function () {
            that.attr('src', img.src);
        }; 
        img.onerror = function () {
            that.replaceWith("<div class='img-thumbnail'>Invalid path " + img_src + "</div>");
        };
        img.src = img_src;
    } else if (!this.is('img')){
        console.log(this, 'plugin is only supposed to be used with img elements');
    } else {
        console.log(this, 'missing data attribute img');
    }
};