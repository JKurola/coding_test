#Coding Test#

##Running the app##

Clone it from ssh://git@bitbucket.org/JKurola/coding_test.git
navigate to folder and run npm install
Call *node server.js* and navigate to localhost:3000 to test the app

##Tests##

Test can be run by calling *npm test*. It includes 4 tests to see if route redirects and response codes are as expected

##Grunt##

Gruntfile is used to combine css and js files into 1 file each

##Custom JQuery##

Custom jqurey plugin is located in public/js/jsplugin.js. It validates path to photo files and reports invalid ones making it easier to detect broken paths

##Choices##

Front-end is done using bootstrap 3 as I'm most familiar with it.
You could achieve same results with any grid based framework

Handlebars is used for templating. There's one main template which is common for every page
and separate files to specify page specific views.


