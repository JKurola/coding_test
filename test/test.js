let assert = require('assert');
let request = require('supertest');

describe('Route Tests', function () {
    let server;
    this.timeout(5000);
    
    beforeEach(function () {
        server = require('../server');
    });
    afterEach(function (done) {
        server.close();
        setTimeout(done, 1000);
    });
    it('responds 404 to non-existing path', function (done) {
        request(server)
        .get('/nonexisting')
        .expect(404, done);
    });
    it('reponds to existing path', function (done) {
        request(server)
        .get('/login')
        .expect(200, done);
    });
    it('profile redirects', function (done) {
        request(server)
        .get('/profile')
        .expect(302, done);
    });
    it('unlogged user gets redirected to login', function (done) {
        request(server)
        .get('/profile')
        .end(function (err, res) {
            if (err) return done(err);
            assert.equal(res.headers.location, '/login');
            done();  
        });
    });
});
