let exphbs = require('express-handlebars');
let express = require("express");
let passport = require('passport');
let OAuth2Strategy = require('passport-oauth2');
let request=require('request');
let handlebars = require('handlebars');
let session = require('express-session');

let oauth_handler = (accessToken, refreshToken, profile, cb) => {
    let options = {
        url: 'https://staging-auth.wallstreetdocs.com/oauth/userinfo',
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'Cache-Control': 'no-cache'
        }
    };
    request.get(options, function(err,res,body){
        if (res.statusCode !== 200) {
            err = 'Unauthorized';
        }
        return cb(err, body);
    });
}

let oauth_strategy = new OAuth2Strategy({
        authorizationURL: 'https://staging-auth.wallstreetdocs.com/oauth/authorize',
        tokenURL: 'https://staging-auth.wallstreetdocs.com/oauth/token',
        clientID: 'coding_test',
        clientSecret: 'bwZm5XC6HTlr3fcdzRnD',
        callbackURL: "http://localhost:3000"
    }, oauth_handler);

module.exports = function (app) {
    handlebars.registerHelper('equal', require('handlebars-helper-equal'));

    app.engine('handlebars', exphbs({defaultLayout: 'main'}));
    app.set('view engine', 'handlebars');

    app.use(express.static('public'));

    passport.use(oauth_strategy);
    app.use(session({secret: 'testsecret'}));
    app.use(passport.initialize());
    app.use(passport.session());
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
};
