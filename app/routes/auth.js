let express = require('express')
let router = express.Router()
let passport = require("passport");

router.get('/wsd', passport.authenticate('oauth2'));

module.exports = router;
