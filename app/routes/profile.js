let express = require('express')
let router = express.Router()

router.use((req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
       res.redirect('/login');
    }
});

router.get('/', (req, res) => {
    res.render('profile', JSON.parse(req.user));
});

module.exports = router;
