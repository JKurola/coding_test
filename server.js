let express = require('express');
let app = express();
let passport = require('passport');
let config = require('./app/config');
let auth_route = require('./app/routes/auth');
let profile_route = require('./app/routes/profile');

config(app);

app.get('/', passport.authenticate('oauth2', { failureRedirect: '/login' }), (req, res) => {
    res.redirect('/profile');
});

app.use('/auth', auth_route)

app.get('/login', (req, res) => {
    res.render('login');
});

app.use('/profile', profile_route)

app.use((req, res) => {
    res.status(404).render('404');
});

let server = app.listen(3000, () => {
    console.log('up and running at 3000');
});
module.exports = server;
